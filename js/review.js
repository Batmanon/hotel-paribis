// Avis tabs

// Tab HOTEL
document.querySelector('#tab-hotel').addEventListener('click', () => {
  document.querySelector('#tab-hotel').style.fontWeight = 'bold';
  document.querySelector('#tab-rooms').style.fontWeight = 'normal';
  document.querySelector('#tab-restau').style.fontWeight = 'normal';
  document.querySelector('#tab-semi').style.fontWeight = 'normal';

  document.querySelector('#review-hotel').style.display = 'block';
  document.querySelector('#review-rooms').style.display = 'none';
  document.querySelector('#review-restau').style.display = 'none';
  document.querySelector('#review-semi').style.display = 'none';
});

// Tab ROOMS
document.querySelector('#tab-rooms').addEventListener('click', () => {
  document.querySelector('#tab-hotel').style.fontWeight = 'normal';
  document.querySelector('#tab-rooms').style.fontWeight = 'bold';
  document.querySelector('#tab-restau').style.fontWeight = 'normal';
  document.querySelector('#tab-semi').style.fontWeight = 'normal';

  document.querySelector('#review-hotel').style.display = 'none';
  document.querySelector('#review-rooms').style.display = 'block';
  document.querySelector('#review-restau').style.display = 'none';
  document.querySelector('#review-semi').style.display = 'none';
});

// Tab RESTAU
document.querySelector('#tab-restau').addEventListener('click', () => {
  document.querySelector('#tab-hotel').style.fontWeight = 'normal';
  document.querySelector('#tab-rooms').style.fontWeight = 'normal';
  document.querySelector('#tab-restau').style.fontWeight = 'bold';
  document.querySelector('#tab-semi').style.fontWeight = 'normal';

  document.querySelector('#review-hotel').style.display = 'none';
  document.querySelector('#review-rooms').style.display = 'none';
  document.querySelector('#review-restau').style.display = 'block';
  document.querySelector('#review-semi').style.display = 'none';
});

// Tab SEMI
document.querySelector('#tab-semi').addEventListener('click', () => {
  document.querySelector('#tab-hotel').style.fontWeight = 'normal';
  document.querySelector('#tab-rooms').style.fontWeight = 'normal';
  document.querySelector('#tab-restau').style.fontWeight = 'normal';
  document.querySelector('#tab-semi').style.fontWeight = 'bold';

  document.querySelector('#review-hotel').style.display = 'none';
  document.querySelector('#review-rooms').style.display = 'none';
  document.querySelector('#review-restau').style.display = 'none';
  document.querySelector('#review-semi').style.display = 'block';
});