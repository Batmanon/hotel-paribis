
// Slider

document.querySelector('#slider-left').addEventListener('click', () => {
  const activeImage = document.querySelector('.slider-content .active');

  if (!activeImage.previousElementSibling)
    return;

  document.querySelector('#slider-right').style.display = 'inline';
  activeImage.previousElementSibling.classList.toggle('active');
  activeImage.classList.toggle('active');

  if (!activeImage.previousElementSibling.previousElementSibling) {
    document.querySelector('#slider-left').style.display = 'none';
  }
});

document.querySelector('#slider-right').addEventListener('click', () => {
  const activeImage = document.querySelector('.slider-content .active');

  if (!activeImage.nextElementSibling)
    return;

  document.querySelector('#slider-left').style.display = 'inline';
  activeImage.nextElementSibling.classList.toggle('active');
  activeImage.classList.toggle('active');

  if (!activeImage.nextElementSibling.nextElementSibling) {
    document.querySelector('#slider-right').style.display = 'none';
  }
});

